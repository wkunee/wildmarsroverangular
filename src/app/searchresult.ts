import {Photo} from './photo';

export class SearchResult {
  amountPhotos: number;
  photos: Photo[];
}
