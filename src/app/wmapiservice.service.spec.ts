import { TestBed } from '@angular/core/testing';

import { WmapiserviceService } from './wmapiservice.service';

describe('WmapiserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WmapiserviceService = TestBed.get(WmapiserviceService);
    expect(service).toBeTruthy();
  });
});
