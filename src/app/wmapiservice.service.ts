import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SearchResult} from './searchresult';
import {Observable, ReplaySubject} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WmapiserviceService {
  public searchResult$: ReplaySubject<SearchResult> = new ReplaySubject<SearchResult>(1);
  private searchResult: SearchResult;
  private searchURL = 'http://localhost:8090/api/search';

  constructor(private http: HttpClient) {
  }

  getResult(sol: number, cam: string): Observable<SearchResult> {
    console.log('calling api');
    return this.http.get<SearchResult>(this.searchURL + '?sol=' + sol + '&cam=' + cam + '&page=1')
      .pipe(
        tap((data: SearchResult) => {
          console.log('data = ' + data);
          this.searchResult = data;
          this.searchResult$.next(this.searchResult);
        })
      );

  }
}
