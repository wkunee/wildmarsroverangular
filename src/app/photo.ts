export class Photo {
  id: number;
  maxSol: number;
  maxDate: string;
  imgSrc: string;
}
