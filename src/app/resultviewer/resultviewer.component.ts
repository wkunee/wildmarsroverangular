import {Component, OnInit} from '@angular/core';
import {SearchResult} from '../searchresult';
import {WmapiserviceService} from '../wmapiservice.service';

@Component({
  selector: 'app-resultviewer',
  templateUrl: './resultviewer.component.html',
  styleUrls: ['./resultviewer.component.css']
})
export class ResultviewerComponent implements OnInit {
  searchResult: SearchResult;
  sol: number;
  cam: string;


  constructor(private service: WmapiserviceService) {
    this.sol = 1000;
    this.cam = 'NAVCAM';
  }

  ngOnInit() {
    this.service.searchResult$
      .subscribe(data => this.searchResult = data);
    this.onUpdate();
  }

  onUpdate(): void {
    console.log('sol is ' + this.sol);
    this.service.getResult(this.sol, this.cam)
      .subscribe();
  }

}
