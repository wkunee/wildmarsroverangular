import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultviewerComponent } from './resultviewer.component';

describe('ResultviewerComponent', () => {
  let component: ResultviewerComponent;
  let fixture: ComponentFixture<ResultviewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultviewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultviewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
